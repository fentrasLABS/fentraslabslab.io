# TODO
- Replace footer links to one leading to About page and put credits/copyright/license/disclaimer
- Separate donate page to archive.org (as a thank you for hosting my projects)
- Compile other abandoned projects (or at least make screenshots for a post/blog)
- Change theme (colors) to a slightly different one?
- Add separate thumbnail for the page itself
- Update categories and tags for KDE subcategory
- Add multiple authors to certain posts (if needed, e.g. KDE)