# Website Guidelines
- Images should be in `JPEG` format (*Hugo* doesn't handle `WebP` well)
- Resolution of `1600x900` must be used for covers and `1920x1080` for thumbnails
- Utilize `hideSummary: true` if entry doesn't have inner description