---
author: "SkyMidnight"
title: "Blockz"
date: "2015-08-17"
description: "Build anything with cubes and blow it up"
categories: ["Games"]
tags: ["abstract", "sandbox", "building", "creative", "mini", "android"]
cover:
  image: "images/cover.jpg"
  relative: true
---

Use your imagination and create anything using colorful cubes. Apply *TNT* to your creations to spice things up (and explode everything)!

<!--more-->

## Screenshots
{{< gallery match="images/_*" >}}

## Gameplay
{{< youtube S0EYbcMpQOk >}}

## Download
### Android
- [Latest](https://archive.org/download/blockz-skymidnight/Blockz.apk)