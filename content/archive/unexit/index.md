---
author: "SkyMidnight"
title: "UnExit"
date: "2013-01-27"
description: "Mysterious maze without any way out"
categories: ["Games"]
tags: ["horror", "adventure", "SCP", "short", "CC-BY-SA", "windows", "android"]
cover:
  image: "images/cover.jpg"
  relative: true
---

You got yourself into an unknown maze and there's only one way to escape — find the exit. But don't forget that you are not alone in here...

<!--more-->

## Screenshots
{{< gallery match="images/_*" >}}

## Gameplay
{{< youtube CVOsl3EARVQ >}}

## Download
### Windows
- [Latest](https://archive.org/download/unexit-unexisted-skymidnight/UnExit.zip)

### Android
- [Latest](https://archive.org/download/unexit-unexisted-skymidnight/UnExit.apk)
- [Preview](https://archive.org/download/unexit-unexisted-skymidnight/UnExit%20Old.apk)

---

Based on **SCP-432** article by *evilscary*, from the [SCP Wiki](https://scp-wiki.wikidot.com/scp-432). Licensed under [CC-BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/).