---
author: "SkyMidnight"
title: "UnExisted"
date: "2014-11-03"
description: "A sequel to UnExit"
categories: ["Games"]
tags: ["horror", "adventure", "unfinished", "SCP", "short", "CC-BY-SA", "windows"]
cover:
  image: "images/cover.jpg"
  relative: true
---

Yet another unknown maze, but five times bigger and with a dragon ending.

<!--more-->

## Screenshots
{{< gallery match="images/_*" >}}

## Gameplay
{{< youtube FwkDlM_z1lQ >}}

## Download
### Windows
- [Latest](https://archive.org/download/unexit-unexisted-skymidnight/UnExisted.zip)

---

Based on **SCP-432** article by *evilscary*, from the [SCP Wiki](https://scp-wiki.wikidot.com/scp-432). Licensed under [CC-BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/).