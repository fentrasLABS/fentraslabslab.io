---
author: "SkyMidnight"
title: "MindCraft SkyMod"
date: "2013-09-19"
description: "Modification for a 2D Minecraft-like homebrew for PlayStation Portable"
categories: ["Mods"]
tags: ["mod", "sandbox", "building", "creative", "minecraft", "PlayStation Portable"]
cover:
  image: "images/cover.jpg"
  relative: true
---

A modification of **MindCraft** homebrew game for PlayStation Portable that brings lots of new features like day&night cycle and creative mode.

<!--more-->

## Screenshots
{{< gallery match="images/_*" >}}

## Download
### PlayStation Portable
- [Latest](https://archive.org/download/mindcraft-skymod/MindCraft%20SM%20Beta%20%28Final%29.zip)

---

Original game by [perli55](https://wololo.net/talk/viewtopic.php?t=4481).