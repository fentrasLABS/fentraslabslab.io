---
author: "SkyMidnight"
title: "SCP-087-B"
date: "2015-07-05"
description: "Dive deep into the unknown hallways and staircases"
categories: ["Games"]
tags: ["horror", "adventure", "SCP", "short", "CC-BY-SA", "android"]
cover:
  image: "images/cover.jpg"
  relative: true
---

> You find yourself inside a set of randomly generated dark hallways and staircases with something lurking below you, and the only way you can go is deeper into the darkness. How deep can you go?

<!--more-->

**SCP-087-B** is a short, experimental indie horror game created by Joonas ("Regalis") Rikkonen. It is loosely based upon the *SCP Foundation* universe and is considered by *Regalis* to be the stepping stone to the creation of *SCP — Containment Breach*.

## Screenshots
{{< gallery match="images/_*" >}}

## Gameplay
{{< youtube KpLPDqJ1cRs >}}

## Download
### Android
- [Google Play](https://play.google.com/store/apps/details?id=com.skymidnight.scp087b)
- [Latest](https://archive.org/download/scp-087-b-skymidnight/SCP-087-B.apk)

---

- Original port by [Rexet Studio](https://rexetstudio.com)
- Original game by [Regalis](https://www.scpcbgame.com/scp-087-b.html)
- Based on **SCP-087** article by *Zaeyde*, from the [SCP Wiki](https://scp-wiki.wikidot.com/scp-087). Licensed under [CC-BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/).