---
author: "SkyMidnight"
title: "The Stairwell"
date: "2013-05-12"
description: "Abandoned staircase that leads to dark places"
categories: ["Games"]
tags: ["horror", "adventure", "SCP", "short", "CC-BY-SA", "windows", "android"]
cover:
  image: "images/cover.jpg"
  relative: true
---

You find an abandoned entrance in the middle of a field. Upon entering it, the door behind closes shut and now your only choice is to go deeper...

<!--more-->

## Screenshots
{{< gallery match="images/_*" >}}

## Gameplay
{{< youtube S8iPhqZulvc >}}

## Download
### Windows
- [Latest](https://archive.org/download/the-stairwell-skymidnight/The%20Stairwell%20%28Windows%29.zip)

### Android
- [Latest](https://archive.org/download/the-stairwell-skymidnight/The%20Stairwell.apk)
- [Preview](https://archive.org/download/the-stairwell-skymidnight/The%20Stairwell%20Old.apk)

---

Based on **SCP-087** article by *Zaeyde*, from the [SCP Wiki](https://scp-wiki.wikidot.com/scp-087). Licensed under [CC-BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/).