---
author: "SkyMidnight"
title: "osu! + Unity"
date: "2018-04-02"
description: "osu! map parser for Unity game engine"
categories: ["Software"]
tags: ["source", "code", "parser", "osu", "C#", "Unity"]
cover:
  image: "images/cover.jpg"
  relative: true
---

**Unity Asset Package** containing tools to parse maps from *osu!* game in *Unity 5* game engine. Doesn't include parsing of sliders and spinners.

<!--more-->

## Screenshots
{{< gallery match="images/_*" >}}

## Gameplay
{{< youtube nQFNPG0XW5Q >}}

## Download
### Unity Package
- [Latest](https://archive.org/download/osu-unity-map-parser/osu_unity-mp-20180402.tar.gz)

---

Original game by [peppy](https://osu.ppy.sh).