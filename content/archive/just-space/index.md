---
author: "SkyMidnight"
title: "Just Space"
date: "2014-12-07"
description: ""
categories: ["Games"]
tags: ["abstract", "experiment", "mini", "windows", "android", "mac", "VR"]
cover:
  image: "images/cover.jpg"
  relative: true
---

> Have you ever felt mad, sad, upset, even more mad? Have you ever wanted to be relaxed or just wanted to fly in space and look at the rainbow? If you said "Yes" to any of those questions, this game is for you! There's no goal in this game, you just fly in space, nothing more...

<!--more-->

A small experiment game where you fly straight and leave a colored trail behind. This simple mechanic can turn your movements into a work of art.


## Screenshots
{{< gallery match="images/_*" >}}

## Gameplay
{{< youtube 3guS8l6ZR7E >}}

## Download
### Windows
- [Latest](https://archive.org/download/just-space-skymidnight/JustSpace1.2-Windows.zip)
- [Legacy](https://archive.org/download/just-space-skymidnight/Just%20Space%20Final.zip)
- [Preview](https://archive.org/download/just-space-skymidnight/Just%20Space.zip)

### Android
- [Latest](https://archive.org/download/just-space-skymidnight/Just%20Space.apk)
- [VR](https://archive.org/download/just-space-skymidnight/Just%20Space%20VR.apk)

### Mac OS
- [Latest](https://archive.org/download/just-space-skymidnight/JustSpace1.2-Mac.zip)
- [Legacy](https://archive.org/download/just-space-skymidnight/JustSpace1.0-Mac.zip)