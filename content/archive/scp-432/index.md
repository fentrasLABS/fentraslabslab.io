---
author: "SkyMidnight"
title: "SCP-432"
date: "2012-12-10"
description: "Surf the unknown tunnels of a cabinet maze in search of lost samples"
categories: ["Games"]
tags: ["horror", "adventure", "SCP", "short", "CC-BY-SA", "windows", "android", "mac"]
cover:
  image: "images/cover.jpg"
  relative: true
---

> **SCP-432** is a 2-door steel storage cabinet, measuring 2 meters tall by 1.2 meters wide by 1 meter deep. The exterior of the cabinet is painted matte green and bears no remarkable features except small areas of corrosion and light scratching commensurate with being left exposed to the elements for a prolonged period of time. The doors of the cabinet are fitted with a basic slide-bolt and a hasp for a padlock, allowing the door to be secured from outside.

<!--more-->

## Screenshots
{{< gallery match="images/_*" >}}

## Gameplay
{{< youtube CA_hd3hGK2c >}}

## Download
### Windows
- [Version 2.0](https://archive.org/download/scp-432-skymidnight/SCP-432%202.0.zip)
- [Version 1.4](https://archive.org/download/scp-432-skymidnight/SCP-432%20v1.4.zip)
- [Version 1.3](https://archive.org/download/scp-432-skymidnight/SCP-432%20v1.3.zip)
- [Version 1.2](https://archive.org/download/scp-432-skymidnight/SCP-432%20v1.2.zip)
- [Version 1.1](https://archive.org/download/scp-432-skymidnight/SCP-432%20v1.1.zip)
- [Version 1.0](https://archive.org/download/scp-432-skymidnight/SCP-432%20v1.0.zip)
- [Monster Test Version](https://archive.org/download/scp-432-skymidnight/SCP-432%20Monster%20Test%20Version.zip)
- [Map Test Version](https://archive.org/download/scp-432-skymidnight/SCP-432%20Test%20Map%20Version.zip)

### Android
- [Version 2.0](https://archive.org/download/scp-432-skymidnight/SCP-432%202.0.apk)
- [Version 1.1](https://archive.org/download/scp-432-skymidnight/SCP-432%20V%201.1%20mobile.apk)

### Mac OS
- [Version 1.1](https://archive.org/download/scp-432-skymidnight/SCP-432%20v1.1%20mac-os.zip)

---

Based on **SCP-432** article by *evilscary*, from the [SCP Wiki](https://scp-wiki.wikidot.com/scp-432). Licensed under [CC-BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/).