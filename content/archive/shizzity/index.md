---
author: "SkyMidnight"
title: "Shizzity"
date: "2013-11-03"
description: "Shoot angry boxes that are coming towards you"
categories: ["Games"]
tags: ["abstract", "quick", "shooter", "action", "mini", "windows"]
cover:
  image: "images/cover.jpg"
  relative: true
---

Destroy angry boxes, collect bonuses and progress through various themes after reaching certain scores.

<!--more-->

## Screenshots
{{< gallery match="images/_*" >}}

## Gameplay
{{< youtube iAdHHJ9_65Q >}}

## Download
### Windows
- [Latest](https://archive.org/download/shizzity-skymidnight/Shizzity.zip)