---
author: "SkyMidnight"
title: "Dreamless"
date: "2013-02-05"
description: "Chambers full of puzzles and secrets"
categories: ["Games"]
tags: ["abstract", "puzzle", "short", "windows"]
cover:
  image: "images/cover.jpg"
  relative: true
---

An abstract game where your goal is to go forward, solve a lot of puzzles and maybe even uncover some mysteries?

<!--more-->

## Screenshots
{{< gallery match="images/_*" >}}

## Gameplay
{{< youtube ZIizxnvoaHs >}}

## Download
### Windows
- [Latest](https://archive.org/download/dreamless-skymidnight/Dreamless%20V1.zip)
- [Preview](https://archive.org/download/dreamless-skymidnight/The%20Dreamless%20%28Demonstration%20Version%29.zip)

---

Inspired by [Antichamber](http://www.antichamber-game.com).