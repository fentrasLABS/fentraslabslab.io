---
author: "SkyMidnight"
title: "Rotationality & Extremlination"
date: "2013-05-07"
description: "Two simple level packs for Open Hexagon"
categories: ["Workshop"]
tags: ["open-hexagon", "quick", "user generated content", "workshop", "steam"]
cover:
  image: "images/cover.jpg"
  relative: true
---

One of the first **Open Hexagon** level packs, *Rotationality* (somehow) being the most popular in the early days of Open Hexagon. Surf through original levels filled with different songs, styles and patterns.

<!--more-->

> Replay the remastered levels that you remembered a long time ago, along with a few changes to give the a bit more spice in the modern day of Open Hexagon.

## Screenshots
{{< gallery match="images/_*">}}

## Gameplay

### Rotationality
{{< youtube qE-YFRgptAk >}}

### Extremlination
{{< youtube 0jtg7gFlUJw >}}

## Download
### Open Hexagon 2.0
- [Rotationality (Steam)](https://steamcommunity.com/sharedfiles/filedetails/?id=2169170656)
- [Extremlination (Steam)](https://steamcommunity.com/sharedfiles/filedetails/?id=2758994773)
- [Rotationality](https://archive.org/download/rotationality-extremlination-open-hexagon/Rotationality%20%28Remastered%29.zip)
- [Extremlination](https://archive.org/download/rotationality-extremlination-open-hexagon/Extremlination%20%28Remastered%29.zip)

### Open Hexagon 1.92
- [Rotationality](https://archive.org/download/rotationality-extremlination-open-hexagon/Rotationality.zip)
- [Extremlination](https://archive.org/download/rotationality-extremlination-open-hexagon/Extremlination.zip)

---

Remastered versions are created by the [community](https://discord.me/openhexagon).