---
author: "SkyMidnight"
title: "MinePhysics"
date: "2013-02-20"
description: "Voxel sandbox with 2D and 3D modes"
categories: ["Games"]
tags: ["sandbox", "building", "creative", "minecraft", "android"]
cover:
  image: "images/cover.jpg"
  relative: true
---

Simple mini sandbox game where you can build something with actual physics. Oh, and don't forget about special **TNT** block to blow everything up!

<!--more-->

## Screenshots
{{< gallery match="images/_*" rowHeight=200 >}}

## Gameplay
{{< youtube KnB2eXZFONs >}}

## Download
### Android
- [Latest](https://archive.org/download/minephysics-skymidnight/MinePhysics.apk)
- [Second Beta](https://archive.org/download/minephysics-skymidnight/MinePhysics%20Second%20Beta.apk)
- [First Beta](https://archive.org/download/minephysics-skymidnight/MinePhysics%20First%20Beta.apk)

---

Inspired by [Minecraft](https://minecraft.net).