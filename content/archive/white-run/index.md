---
author: "SkyMidnight"
title: "White Run"
date: "2013-07-28"
description: "Quickly tap out green boxes while flying through a tunnel"
categories: ["Games"]
tags: ["abstract", "quick", "action", "mini", "OST", "android"]
cover:
  image: "images/cover.jpg"
  relative: true
---

Tap on green boxes and collect useful bonuses! Can you reach 750 points and unlock a special mode?

<!--more-->

## Screenshots
{{< gallery match="images/_*" >}}

## Soundtrack
{{< youtube 3WuEcTe2L_E >}}

## Download
### Android
- [Latest](https://archive.org/download/white-run-skymidnight/White%20Run.apk)

---

Original soundtrack by [MrModez](https://www.youtube.com/user/MrModez).