---
author: ["Skye Fentras", "eiglow", "KDE Promo Team"]
title: "Plasma 5.20"
date: "2020-10-13"
description: "Promotional video for Plasma 5.20"
categories: ["Videos"]
tags: ["KDE", "Plasma", "announcement", "promo"]
cover:
  image: "images/cover.jpg"
  picture: "images/thumbnail.jpg"
  relative: true
---

> A massive release, containing improvements to dozens of components, widgets, and the desktop behavior.

<!--more-->

Promotional video for the announcement of [Plasma 5.20](https://kde.org/announcements/plasma/5/5.20.0/) made in [Blender](https://blender.org).

{{< youtube h_YdaFNsPBE >}}

---

- Official wallpaper from *Plasma 5.20*
- Music by [eiglow](https://soundcloud.com/eiglow)