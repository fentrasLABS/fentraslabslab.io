---
author: "Skye Fentras"
title: "Plasma 2020"
date: "2020-03-25"
description: "Contest promoting KDE and Plasma sponsored by Tuxedo Computers"
categories: ["Videos"]
tags: ["KDE", "Plasma", "announcement", "promo", "competition"]
cover:
  image: "images/cover.jpg"
  picture: "images/thumbnail.jpg"
  relative: true
---

> Winner of the *Plasma Video Competition*, **Skye Fentras** shows off *Plasma's* spectacular features in their *Plasma 2020* video.

<!--more-->

This was a contest to promote [KDE](https://kde.org) *Plasma* or/and its *applications* where participants had to create a promotional video and show off the most notable features. The video was done in [Blender](https://blender.org) and [Kdenlive](https://kdenlive.org) on a *Dell Inspiron 5378* laptop (with integrated graphics). It took a while to render the whole thing but in the end the video was a success.

{{< youtube u8qT_MhecYo >}}

---

- Music by [Anenon](https://freemusicarchive.org/music/Anenon/Bonus_Beat_Blast_2011/05_anenon-embers_and_ashes/)