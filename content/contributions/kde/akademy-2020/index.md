---
author: ["Skye Fentras", "KDE Promo Team"]
title: "Akademy 2020"
date: "2020-09-05"
description: "Welcome video for Akademy 2020"
categories: ["Videos"]
tags: ["KDE", "Akademy", "talks", "promo"]
cover:
  image: "images/cover.jpg"
  picture: "images/thumbnail.jpg"
  relative: true
---

> Welcome to #Akademy2020! How do you say *"welcome"* in your native language?

<!--more-->

Promotional video for the introduction of [Akademy 2020](https://akademy.kde.org/2020). The video was made entirely in [Kdenlive](https://kdenlive.org) and videos are provided by many people of *KDE* community.

{{< youtube v07r6_6E8TQ >}}

# Akademy Talks
Meanwhile I was also asked to make a thumbnail and an introduction sequence for *Akademy* talks which was later automated by someone I can't remember (sorry). The sequence in the video below is between `0:00` and `0:10`.

{{< youtube iTsANIujfDk >}}

---

- Videos and photos provided by *KDE* community
- Music by [Chalk Dinosaur](https://chalkdinosaur.bandcamp.com/track/studio-4)