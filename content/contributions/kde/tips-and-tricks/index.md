---
author: ["Skye Fentras", "KDE Promo Team"]
title: "Tips and Tricks"
date: "2021-01-01"
description: "Various tips and tricks about Plasma and KDE applications"
categories: ["Videos"]
tags: ["KDE", "Plasma", "promo", "short"]
cover:
  image: "images/cover.jpg"
  picture: "images/thumbnail.jpg"
  relative: true
---

Useful tips and tricks for *Plasma* and *KDE* applications on various social media. Made in [Blender](https://blender.org) and [Kdenlive](https://kdenlive.org).

<!--more-->

## Mouse Wheel to Control Volume
{{< youtube izbPzGL8TgI >}}

## Meta Keys to Move Windows
{{< youtube 8s2Jvrp2HyA >}}

---

- Thumbnail from [KDE YouTube Channel](https://www.youtube.com/watch?v=aK6TG6Ekhic)
- Music by [Kevin Macleod](https://incompetech.com)
- *Low Poly Laptop* model by [Abhishek Verma](https://sketchfab.com/3d-models/low-poly-laptop-48cb9699178447f284f15c8ef1d47f6b)
- *Snow Close Up* video by [mattio](https://www.videvo.net/video/snow-close-up/3318/)
- *Snow Fall* by [Videvo](https://www.videvo.net/video/snow-fall/427/)