---
author: ["Skye Fentras", "Niccolo Venerandi", "eiglow", "KDE Promo Team"]
title: "Plasma 5.19"
date: "2020-06-09"
description: "Promotional video for Plasma 5.19"
categories: ["Videos"]
tags: ["KDE", "Plasma", "announcement", "promo"]
cover:
  image: "images/cover.jpg"
  picture: "images/thumbnail.jpg"
  relative: true
---

> In this release, we have prioritized making Plasma more consistent, correcting and unifying designs of widgets and desktop elements; worked on giving you more control over your desktop by adding configuration options to the System Settings; and improved usability, making Plasma and its components easier to use and an overall more pleasurable experience.

<!--more-->

Promotional video for the announcement of [Plasma 5.19](https://kde.org/announcements/plasma/5/5.19.0/) which was made entirely in [Blender](https://blender.org).

{{< youtube trJ0Q-wsna4 >}}

---

- 2D animations by [Niccolo Venerandi](https://niccolo.venerandi.com)
- Music by [eiglow](https://soundcloud.com/eiglow)