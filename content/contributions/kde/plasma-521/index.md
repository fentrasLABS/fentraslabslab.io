---
author: ["Skye Fentras", "Aron Kovacs", "Niccolo Venerandi", "KDE Promo Team"]
title: "Plasma 5.21"
date: "2021-02-16"
description: "Promotional video for Plasma 5.21"
categories: ["Videos"]
tags: ["KDE", "Plasma", "announcement", "promo"]
cover:
  image: "images/cover.jpg"
  picture: "images/thumbnail.jpg"
  relative: true
---

> *Plasma 5.21* is a looker! A new wallpaper, a new theme, and especially a new app launcher make *Plasma* not only easier on the eyes, but also easier to use and much more accessible.

<!--more-->

Promotional video for the announcement of [Plasma 5.21](https://kde.org/announcements/plasma/5/5.21.0/) made in [Blender](https://blender.org) and [Kdenlive](https://kdenlive.org).

{{< youtube ahEWG4JCA1w >}}

---

- 3D design by [Áron Kovács](http://aronkvh.hu)
- Contributions by [Niccolo Venerandi](https://niccolo.venerandi.com)
- Music by [Fluxoid](https://fluxoid.bandcamp.com/track/solitude)