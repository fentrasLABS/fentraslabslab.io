---
author: "fentras LABS"
title: "Borders"
date: "2021-08-21"
description: "Create virtual borders around the screen to imitate arbitrary resolutions or reduce distractions"
hideSummary: true
categories: ["Mods"]
tags: ["Trackmania", "Openplanet", "plugin", "overlay"]
cover:
  image: "images/cover.jpg"
  picture: "images/thumbnail.jpg"
  relative: true
---

## Features
* Colors (opacity included)
* Size (adjusts to widescreen automatically)
* Order (overlay under the in-game UI)

## Limitations
* Borders disappear when the game UI is hidden

## Screenshots
{{< gallery match="images/_*" >}}

## Download
* [Openplanet](https://openplanet.dev/plugin/borders)
* [GitLab](https://gitlab.com/fentrasLABS/openplanet/borders/-/releases)
* [Source](https://gitlab.com/fentrasLABS/openplanet/borders)

---

*Project icon* by [Fork Awesome](https://forkaweso.me)