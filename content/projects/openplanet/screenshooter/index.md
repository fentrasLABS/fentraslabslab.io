---
author: "fentras LABS"
title: "Screenshooter"
date: "2021-12-07"
description: "Enhance in-game screenshot feature and enable custom resolutions, 360 panoramas and more"
hideSummary: true
categories: ["Mods"]
tags: ["Trackmania", "Openplanet", "plugin", "recording"]
cover:
  image: "images/cover.jpg"
  picture: "images/thumbnail.jpg"
  relative: true
---

## Features
* Custom resolution (from *16x16* to *8K* and more)
* 3 output formats 
    * `.webp` (slow, invisible compression, small file size)
    * `.jpg` (fast, visible compression, smallest file size)
    * `.tga` (fastest, lossless, huge file size)
* 360 Panorama (equirectangular)
* Tiling (for super-resolution screenshots)
* Sequential capturing (99 frames maximum, captured each 10th of a second)

## Limitations
* 99 screenshots in the folder will cause overwriting (starting from the first one)
* Extreme resolutions can cause crashes (16K and more)
* Tiling is available only in *ManiaPlanet* (missing top row)
* Panorama mode
    * Available only in *ManiaPlanet* (*Trackmania2020* crashes)
    * Most maps cause flipped tiles (fixable using *Fix Tiles* option sacrificing quality)
    * TARGA (`.tga`) flips *Red* and *Blue* color channels
    * Noticeable seams due to fake lighting

## Screenshots
{{< gallery match="images/_*" >}}

## Download
* [Openplanet](https://openplanet.dev/plugin/screenshooter)
* [GitLab](https://gitlab.com/fentrasLABS/openplanet/screenshooter/-/releases)
* [Source](https://gitlab.com/fentrasLABS/openplanet/screenshooter)

---

Project icon provided by [Fork Awesome](https://forkaweso.me/)