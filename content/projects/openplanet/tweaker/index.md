---
author: "fentras LABS"
title: "Tweaker"
date: "2021-09-21"
description: "Modify in-game hidden settings like Draw Distance and slightly boost the performance"
hideSummary: true
categories: ["Mods"]
tags: ["Trackmania", "Openplanet", "plugin", "performance", "tweaks"]
cover:
  image: "images/cover.jpg"
  picture: "images/thumbnail.jpg"
  relative: true
---

## Features
* Graphics
  * Screen Resolution
  * Draw Distance
  * Rendering
  * Projectors
* Environment
  * Decoration
  * Full SkyDome
  * Clouds
* Camera
  * Field of View
  * Aspect Ratio
  * Stereoscopy
* Interface
  * Overlay Scaling
  * FPS counter
* Special
  * Wipeout Mode
  * Quick Zoom
* System
  * `Default.json` Editor

## Screenshots
{{< gallery match="images/_*" >}}

## Download

### Stable
* [Openplanet](https://openplanet.dev/plugin/tweaker)
* [GitLab](https://gitlab.com/fentrasLABS/openplanet/tweaker/-/releases)
* [Source](https://gitlab.com/fentrasLABS/openplanet/tweaker)

### Preview
* [Openplanet](https://openplanet.dev/plugin/tweakerpreview)
* [Source](https://gitlab.com/fentrasLABS/openplanet/tweaker/-/tree/preview)

---

- *Signature fix* by [XertroV](https://gitlab.com/XertroV/tweaker/-/commit/165cc8f690ba66d97c9aba5a53a40d6dc2f8f957)
- *UI tabs* by [Miss](https://github.com/openplanet-nl/plugin-manager/tree/master/src/Interface)
- *Thumbnail background* by [blackpulse](https://trackmania.exchange/maps/54009)
- *Project icon* by [Fork Awesome](https://forkaweso.me/)