---
author: "fentras LABS"
title: "Haptics"
date: "2022-08-22"
description: "Attach external haptic devices to vehicle parameters (RPM, speed, gear, etc.) and monitor their states"
hideSummary: true
categories: ["Mods"]
tags: ["Trackmania", "Openplanet", "plugin", "hardware"]
cover:
  image: "images/cover.jpg"
  picture: "images/thumbnail.jpg"
  relative: true
---

## Disclaimer
This plugin is in *proof-of-concept* state. I am not responsible for any possible damages or unwanted actions this plugin may bring. Be careful of your surroundings and use at your own risk!

## Installation
- Download and run [Intiface Desktop](https://github.com/intiface/intiface-desktop/releases) and [Haptics Web Bridge](https://openplanet.dev/file/102).
- Turn on your devices (connect manually if needed).
- You can manually specify the address, port and key in the plugin settings *(Openplanet > Settings > Haptics > Server)*.
- Update devices in the plugin, change their settings and have fun!

Please refer to the [Installation](https://gitlab.com/fentrasLABS/openplanet/haptics/-/wikis/Installation) page for complete guide.

## Screenshots
{{< gallery match="images/_*" >}}

## Download

### Plugin
* [Openplanet](https://openplanet.dev/plugin/haptics)
* [GitLab](https://gitlab.com/fentrasLABS/openplanet/haptics/-/releases)
* [Source](https://gitlab.com/fentrasLABS/openplanet/haptics)

### Web Bridge
* [Openplanet](https://openplanet.dev/file/102)
* [Source](https://gitlab.com/fentrasLABS/openplanet/haptics-web-bridge)

---

- *Web Bridge* by [Kyrah Abattoir](https://gitlab.com/KyrahAbattoir)
- *Project icon* by [Kenney Game Icons](https://kenney.nl/assets/game-icons)
- *UI tabs* by [Miss Plugin Manager](https://github.com/openplanet-nl/plugin-manager/tree/master/src/Interface)
